# Manage NTP
class puppet-ntp {
  ensure_pagages(['ntp'])

  file { '/etc/ntp/conf':
    source  => 'puppet://modules/puppet-ntp/ntp.conf',
    notify  => Service['ntp'],
    require => Package['ntp'],
  }

  service { 'ntp':
    ensure => running,
    enable => true,
  }
}
